<?php

namespace kfit\notifications\helpers;

use Yii;

/**
 * Helper para el almacenado de todas las constantes del sistema
 *
 * @package app
 * @subpackage helpers
 * @category helpers
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @copyright TicMakers 2020
 */
class Constants
{
    const TYPE_NOTIFICATION = [
        'E' => 'Email',
        'P' => 'Push notification'
    ];

    const STATUS_NOTIFICATION = [
        'P' => 'Pending',
        'F' => 'Failed',
        'S' => 'Successful'
    ];
}
