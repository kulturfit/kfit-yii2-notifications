<?php
namespace kfit\notifications\migrations;
class m190221_210102_NotificationRecipients extends \yii\db\Migration
{
    public $tableName = 'notification_recipients';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'notification_recipient_id' => $this->primaryKey(),
            'notification_id' => $this->integer()->notNull(),
            'recipient' => $this->string(128)->notNull(),
            'user_id' => $this->integer(),
            'status' => $this->char(1)->notNull(),
            'status_information' => $this->text(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'FOREIGN KEY ([[notification_id]]) REFERENCES notifications ([[notification_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'notification_recipient_id', "Records's unique identifier");

        $this->addCommentOnColumn($this->tableName, 'notification_id', "Notification linked to the recipient");
        $this->addCommentOnColumn($this->tableName, 'recipient', "Destination to which the notification will arrive. It can be an email, a cell phone number, a device identifier, etc.");
        $this->addCommentOnColumn($this->tableName, 'user_id', "System's user associated with the recipient. This only applies in the case that the recipient has a user within the system");
        $this->addCommentOnColumn($this->tableName, 'status', "Notification's status for the recipient. Can take the values ​​P: Pending send; S: Successful send; F: Failed send");
        $this->addCommentOnColumn($this->tableName, 'status_information', "Additional information about the state. Example, in case of failure here goes the description of the fault.");

        $this->addCommentOnColumn($this->tableName, 'active', "Indicates whether the record is active or not");
        $this->addCommentOnColumn($this->tableName, 'created_by', "User's id who created the record");
        $this->addCommentOnColumn($this->tableName, 'created_at', "Date and time the record was created");
        $this->addCommentOnColumn($this->tableName, 'updated_by', "Last user's id who modified the record");
        $this->addCommentOnColumn($this->tableName, 'updated_at', "Date and time of the last modification of the record");

        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"chk-{$this->tableName}.active\"
                        CHECK (active IN ('Y', 'N'))");
        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"chk-{$this->tableName}.status\"
                        CHECK (status IN ('P', 'S', 'F'))");

        $this->addCommentOnTable($this->tableName, "System's notifications recipients");

    }

    public function down()
    {
        $this->dropTable('notification_recipients');
    }
}
