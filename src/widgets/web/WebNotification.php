<?php
namespace kfit\notifications\widgets\web;

use yii\base\Widget;
use yii\helpers\Html;

class WebNotification extends Widget
{
    public $message;

    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = 'Hello World';
        }
    }

    public function run()
    {
        return $this->render('web');
               
    }
}
