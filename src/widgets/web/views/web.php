<?php
use yii\widgets\Pjax;

Pjax::begin(['id' => 'id-pjax', 'clientOptions' => ['global' => false]]);
echo time();
Pjax::end();

$js = 'function refresh() {
    $.ajaxSetup({
        global: false,
        type: "POST"
    });
     $.pjax.reload({container:"#id-pjax"});
     setTimeout(refresh, 5000); // restart the function every 5 seconds
     $.ajaxSetup({
        global: true,
        type: "POST"
    });
 }
 refresh();';
$this->registerJs($js, $this::POS_READY);
