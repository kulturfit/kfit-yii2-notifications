<?php
namespace kfit\notifications\assets;

/**
 * WebChannelAssets Permite la carga de los plugins y dependencias necesarias para el canal de notificaciones Web
 *
 * @package kfit\notifications
 * @category Assets
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class WebChannelAsset extends AssetBundle
{
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yiiassets\bootbox\BootBoxAsset',
    ];
}
