<?php

namespace kfit\notifications\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "notifications".
 * System's notifications
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $notification_id 
 * @property string $type Type notification (W: Web notification; E: Email; M: Text message; P: Push notification)
 * @property string $title Notification's title in case you apply how in emails
 * @property string $message Notification's message
 * @property string $small_icon Small icon that appears on the left side of push notifications
 * @property string $big_icon Large icon that appears in push notifications
 * @property string $additional_information Additional information used to transmit data to the application (Not visible by the user). It can be any text like a json or fix
 * @property string $buttons Array or json with the buttons (identifier, name, icon, action, etc) that will appear in the notification
 * @property string $generated_from Specifies the section or module of the system from which the notification was generated
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Date and time the record was created
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property NotificationRecipients[] $notificationRecipients Datos relacionados con modelo "NotificationRecipients"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class Notifications extends \kfit\core\base\Model
{
    /**
     * Definición del nombre de la tabla.
     *
     * @return string
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['type', 'message'], 'required'],
            [['message', 'additional_information', 'buttons'], 'string'],
            [['type'], 'string', 'max' => 1],
            [['title'], 'string', 'max' => 128],
            [['small_icon', 'big_icon', 'generated_from'], 'string', 'max' => 64],
        ];
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'notification_id' => Yii::t('notifications', 'Código'),
            'type' => Yii::t('notifications', 'Type'),
            'title' => Yii::t('notifications', 'Title'),
            'message' => Yii::t('notifications', 'Message'),
            'small_icon' => Yii::t('notifications', 'Small icon'),
            'big_icon' => Yii::t('notifications', 'Big icon'),
            'additional_information' => Yii::t('notifications', 'Additional information'),
            'buttons' => Yii::t('notifications', 'Buttons'),
            'generated_from' => Yii::t('notifications', 'Generated from'),
            'active' => Yii::t('notifications', 'Active'),
            'created_by' => Yii::t('notifications', 'Created by'),
            'created_at' => Yii::t('notifications', 'Created at'),
            'updated_by' => Yii::t('notifications', 'Updated by'),
            'updated_at' => Yii::t('notifications', 'Updated at'),
        ];
    }

    /**
     * Retorna los textos de las ayudas de los campos.
     *
     * @return array|string
     */
    public function getHelp($attribute = null)
    {
        $helps = [
            'notification_id' => Yii::t('notifications', ''),
            'type' => Yii::t('notifications', 'Type notification (w: web notification; e: email; m: text message; p: push notification)'),
            'title' => Yii::t('notifications', 'Notification\'s title in case you apply how in emails'),
            'message' => Yii::t('notifications', 'Notification\'s message'),
            'small_icon' => Yii::t('notifications', 'Small icon that appears on the left side of push notifications'),
            'big_icon' => Yii::t('notifications', 'Large icon that appears in push notifications'),
            'additional_information' => Yii::t('notifications', 'Additional information used to transmit data to the application (not visible by the user). it can be any text like a json or fix'),
            'buttons' => Yii::t('notifications', 'Array or json with the buttons (identifier, name, icon, action, etc) that will appear in the notification'),
            'generated_from' => Yii::t('notifications', 'Specifies the section or module of the system from which the notification was generated'),
            'active' => Yii::t('notifications', 'Indicates whether the record is active or not'),
            'created_by' => Yii::t('notifications', 'User\'s id who created the record'),
            'created_at' => Yii::t('notifications', 'Date and time the record was created'),
            'updated_by' => Yii::t('notifications', 'Last user\'s id who modified the record'),
            'updated_at' => Yii::t('notifications', 'Date and time of the last modification of the record'),
        ];
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * Definición de la relación con el modelo "NotificationRecipients".
     *
     * @return \app\models\app\NotificationRecipients
     */
    public function getNotificationRecipients()
    {
        $query = $this->hasMany(Yii::$container->get(NotificationRecipients::class), ['notification_id' => 'notification_id']);
        $query->andWhere([static::STATUS_COLUMN => static::STATUS_ACTIVE])->cache(3);
        return $query;
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|Notifications[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::STATUS_COLUMN])) {
            $attributes[static::STATUS_COLUMN] = static::STATUS_ACTIVE;
        }

        if (empty($orderBy)) {
            $orderBy[static::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->cache(3)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query, 'notification_id', static::getNameFromRelations());
        }
        return $query;
    }
}
