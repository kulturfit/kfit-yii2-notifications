<?php

namespace kfit\notifications\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "notification_recipients".
 * System's notifications recipients
 *
 * @package kfit\notifications\models\app\NotificationRecipients 
 *
 * @property integer $notification_recipient_id Records's unique identifier
 * @property integer $notification_id Notification linked to the recipient
 * @property string $recipient Destination to which the notification will arrive. It can be an email, a cell phone number, a device identifier, etc.
 * @property integer $user_id System's user associated with the recipient. This only applies in the case that the recipient has a user within the system
 * @property string $status Notification's status for the recipient. Can take the values ​​P: Pending send; S: Successful send; F: Failed send
 * @property string $status_information Additional information about the state. Example, in case of failure here goes the description of the fault.
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Date and time the record was created
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property Notifications $notification Datos relacionados con modelo "Notifications"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class NotificationRecipients extends \kfit\notifications\models\base\NotificationRecipients
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge(parent::formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }
}
