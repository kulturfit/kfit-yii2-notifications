<?php

namespace kfit\notifications\models\app;

use Yii;

/**
 * Éste es el modelo para la tabla "notifications".
 * System's notifications
 *
 * @package kfit\notifications\models\app 
 *
 * @property integer $notification_id 
 * @property string $type Type notification (W: Web notification; E: Email; M: Text message; P: Push notification)
 * @property string $title Notification's title in case you apply how in emails
 * @property string $message Notification's message
 * @property string $small_icon Small icon that appears on the left side of push notifications
 * @property string $big_icon Large icon that appears in push notifications
 * @property string $additional_information Additional information used to transmit data to the application (Not visible by the user). It can be any text like a json or fix
 * @property string $buttons Array or json with the buttons (identifier, name, icon, action, etc) that will appear in the notification
 * @property string $generated_from Specifies the section or module of the system from which the notification was generated
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Date and time the record was created
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property NotificationRecipients[] $notificationRecipients Datos relacionados con modelo "NotificationRecipients"
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.
 */
class Notifications extends \kfit\notifications\models\base\Notifications
{
    const STATUS_PENDING = 'P';
    const STATUS_FAILED = 'F';
    const STATUS_SENT = 'S';
    const TYPE_EMAIL = 'E';
    const TYPE_PUSH = 'P';

    /**
     * Número de notificaciones pendientes
     *
     * @var int
     */
    public $pending;

    /**
     * Número de notificaciones fallidas
     *
     * @var int
     */
    public $failed;

    /**
     * Número de notificaciones enviadas
     *
     * @var int
     */
    public $sent;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        $rules = [];
        return Yii::$app->arrayHelper::merge(parent::rules(), $rules);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $attributeLabels = [];
        return Yii::$app->arrayHelper::merge(parent::attributeLabels(), $attributeLabels);
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        $newhelps = [];
        $helps = Yii::$app->arrayHelper::merge(parent::getHelp(), $newhelps);
        return !is_null($attribute) && isset($helps[$attribute]) ? $helps[$attribute] : $helps;
    }

    /**
     * @inheritDoc
     */
    public static function getNameFromRelations()
    {
        return parent::getNameFromRelations();
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge(parent::behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $parentFormColums = parent::formColumns();

        $formColumns = [
            'type' => [
                'attribute' => 'type',
                'widget' => [
                    "class" => "kartik\widgets\Select2",
                    "data" => static::getTypes(),
                    "options" => [
                        "placeholder" => Yii::$app->strings::getTextEmpty(),
                    ]
                ],
                'render' => ['C', 'U'],
            ]
        ];
        return Yii::$app->arrayHelper::merge($parentFormColums, $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $gridColumns = [];
        return Yii::$app->arrayHelper::merge(parent::gridColumns(), $gridColumns);
    }

    /**
     * Obtener el listado de estados de las notificaciones
     *
     * @return array | $statusList: listado de estados de las notificaciones
     */
    public static function getStatus()
    {
        return [
            'P' => Yii::t('notifications', 'Pending'),
            'F' => Yii::t('notifications', 'Failed'),
            'S' => Yii::t('notifications', 'Sent')
        ];
    }

    /**
     * Obtener listado de tipos de notificación con su respectiva traducción
     *
     * @return array | $typeList: listado de tipos de notificación
     */
    public static function getTypes()
    {
        return [
            'E' => Yii::t('notifications', 'Email'),
            'P' => Yii::t('notifications', 'Push notification')
        ];
    }
}
