<?php

namespace kfit\notifications\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use kfit\notifications\models\base\NotificationRecipients as NotificationRecipientsModel;

/**
 * Esta clase representa las búsqueda para el modelo `kfit\notifications\models\base\NotificationRecipients`.
 *
 * @package kfit
 * @subpackage notifications/models/searchs
 * @category Models
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class NotificationRecipients extends NotificationRecipientsModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                [
                    'notification_recipient_id',
                    'notification_id',
                    'user_id',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [
                [
                    'recipient',
                    'status',
                    'status_information',
                    'active',
                    'created_at',
                    'updated_at'
                ],
                'safe'
            ]
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotificationRecipientsModel::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'updated_at' => SORT_DESC
                ]
            ]
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        //Condición para filtros
        $query->andFilterWhere([
            'notification_recipient_id' => $this->notification_recipient_id,
            'notification_id' => $this->notification_id,
            'user_id' => $this->user_id,
            'created_by' => $this->created_by,
            'DATE(created_at)' => $this->created_at,
            'updated_by' => $this->updated_by,
            'DATE(updated_at)' => $this->updated_at
        ]);
        $query
            ->andFilterWhere(['like', 'recipient', $this->recipient])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere([
                'like',
                'status_information',
                $this->status_information
            ])
            ->andFilterWhere(['like', 'active', $this->active]);
        return $dataProvider;
    }
}
