<?php
namespace kfit\notifications\controllers;

use Yii;
use kfit\core\widgets\ActiveForm;
use yii\web\Response;
use kfit\core\base\Controller;

/**
 * Controlador NotificationRecipientsController implementa las acciones para el CRUD de el modelo NotificationRecipients.
 *
 * @package kfit\notifications
 * @subpackage controllers
 * @category Controllers
 *
 * @property string $model Ruta del modelo principal.
 * @property string $searchModel Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author Juan Sebastian Muñoz Reyes <sebastianmr302@gmail.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class NotificationRecipientsController extends Controller
{
    public $isModal = true;
    public $modelClass = \kfit\notifications\models\app\NotificationRecipients::class;
    public $searchModelClass = \kfit\notifications\models\searchs\NotificationRecipients::class;

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $instance = Yii::createObject($this->modelClass);
        $paramsGet = Yii::$app->request->get();
        if (!empty($paramsGet)) {
            $instance->setAttributes($paramsGet);
        }
        if ($instance->load(Yii::$app->request->post())) {
            $instance->status = 'P';
            if ($instance->save()) {
                $res['state'] = 'success';
                $res['message'] = Yii::t(
                    $this->module->id,
                    'It was created successfully.'
                );
            } else {
                $res['state'] = 'error';
                $res['message'] = \yii\helpers\Html::errorSummary($instance);
                $res['error'] = ActiveForm::validate($instance);
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $res;
        } else {
            return $this->renderAjax('_form', [
                'model' => $instance
            ]);
        }
    }

    /**
     * Permite realizar el borrado lógico del registro (activo)
     *
     * @param integer $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $instance = $this->findModel($id);
        $instance->{$instance::STATUS_COLUMN} = $instance::STATUS_INACTIVE;
        if ($instance->save(false)) {
            Yii::$app->message::setMessage(
                Yii::$app->message::TYPE_SUCCESS,
                Yii::t($this->module->id, 'Se eliminó con éxito.')
            );
        } else {
            Yii::$app->message::setMessage(
                Yii::$app->message::TYPE_DANGER,
                Yii::t(
                    $this->module->id,
                    'Ocurrió un error al intentar eliminar.'
                )
            );
        }
        return $this->redirect([
            'default/update',
            'id' => $instance->notification_id,
            'tab' => 2
        ]);
    }
}
