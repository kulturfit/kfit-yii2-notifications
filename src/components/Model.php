<?php

namespace kfit\notifications\components;

use kfit\notifications\Module;
/**
 * Modelo base
 *
 * @package kfit
 * @subpackage notifications
 * @category Components
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @copyright Copyright (c) 2018 KulturFit S.A.S.
 *
 */
class Model extends \kfit\core\base\Model
{
   

    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * Configuración inicial.
     *
     * @return null
     */
    public function init()
    {
        parent::init();
        $this->module = Module::getInstance();
    }
}
