<?php

namespace kfit\notifications\components;

use kfit\notifications\models\base\Notifications;
use Yii;
use kfit\notifications\Module;

/**
 * Clase abstracta para el envío por medio de un servicio.
 *
 * @package kfit
 * @subpackage notifications\components
 * @category components
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
abstract class Channel extends \yii\base\BaseObject
{
    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * Tipo de notificación a ser procesada por el canal
     *
     * @var string
     */
    public $type;

    /**
     * Mensaje de error en caso de haberlo.
     *
     * @var string
     */
    public $errorMessage = '';

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->module = Module::getInstance();
        if (is_null($this->type)) {
            throw new \yii\base\InvalidConfigException(
                Yii::t(
                    $this->module->id,
                    "It's necessary to define the type of notification."
                )
            );
        }
    }

    /**
     * Realiza el envío de la notificación por el servicio indicado.
     *
     * @return void
     */
    abstract public function send(Notifications $notification);
}
