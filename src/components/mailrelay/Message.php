<?php

namespace kfit\notifications\components\mailrelay;

use yii\mail\MailerInterface;

class Message extends \yii\swiftmailer\Message
{
    public $errorMessage;
    /**
     * @var MailerInterface the mailer instance that created this message.
     * For independently created messages this is `null`.
     */
    public $mailer;

    /**
     * Sends this email message.
     * @param MailerInterface $mailer the mailer that should be used to send this message.
     * If no mailer is given it will first check if [[mailer]] is set and if not,
     * the "mail" application component will be used instead.
     * @return bool whether this message is sent successfully.
     */
    public function send(MailerInterface $mailer = null)
    {
        $returnValue = true;
        if ($mailer === null && $this->mailer === null) {
            $mailer = Yii::$app->getMailer();
        } elseif ($mailer === null) {
            $mailer = $this->mailer;
        }
        $curl = curl_init("https://{$mailer->hostName}/api/v1/sent_campaigns");

        $rcpt = array(
            array(
                'name' => current(array_values($this->getTo())),
                'email' => current(array_keys($this->getTo()))
            )
        );
        $postData = array(
            'function' => 'sendMail',
            'apiKey' => $mailer->apiKey,
            'subject' => $this->getSubject(),
            'html' => $mailer->html,
            'emails' => $rcpt
        );
        $post = http_build_query($postData);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        if ($json === false) {
            $this->errorMessage =
                'Request failed with error: ' . curl_error($curl);
            $returnValue = false;
        } else {
            $result = json_decode($json);
            if ($result->status == 0) {
                $this->errorMessage =
                    'Bad status returned. Error: ' . $result->error;
                $returnValue = false;
            }
        }
        return $returnValue;
    }
}
