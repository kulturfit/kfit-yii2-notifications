<?php

namespace kfit\notifications\components\channels;

use Yii;
use kfit\notifications\models\base\Notifications;

/**
 * Canal para manejar el envío de notificaciones por correo electrónico via OnesignalChannel.
 *
 * @package kfit
 * @subpackage notifications\components\channels
 * @category components
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class WebChannel extends \kfit\notifications\components\Channel
{
    /**
     * Instancia de onesignal.
     *
     * @var [type]
     */
    protected $dialogConfig;

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $type = 'W';

    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        if (is_null($this->module->onesignalConfig)) {
            throw new \yii\base\InvalidConfigException(
                Yii::t(
                    $this->module->id,
                    "It's necessary to define the configuration to the onesignal component."
                )
            );
        }
        $this->onesignal = Yii::createObject($this->module->onesignalConfig);
    }

    /**
     * Método para el envío de los datos
     *
     * @param Notifications $notifications
     * @return void
     */
    public function send(Notifications $notifications)
    {
    }
}
