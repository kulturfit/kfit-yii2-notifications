<?php

namespace kfit\notifications\components\channels;

use Yii;
use kfit\notifications\models\base\Notifications;
use kfit\notifications\models\base\NotificationRecipients;

/**
 * Canal para manejar el envío de notificaciones via OnesignalChannel.
 *
 * @package kfit
 * @subpackage notifications\components\channels
 * @category components
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class OnesignalChannel extends \kfit\notifications\components\Channel
{
    /**
     * Instancia de onesignal.
     *
     * @var [type]
     */
    protected $onesignal;

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $type = 'P';

    /**
     * Constructor
     *
     * @param [type] $id
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $onesignalConfiguration = $this->module->onesignalConfig;
        if ($this->validateConfigurations()) {
            $this->onesignal = Yii::createObject($onesignalConfiguration);
        }
    }

    /**
     * Método para el envío de los datos de la notificación tipo push
     *
     * @param Notifications $notifications
     * @return boolean
     */
    public function send(Notifications $notification)
    {
        $returnValue = true;
        $statusRecord = [];

        $sql = "
        SELECT nrc.notification_recipient_id, nrc.recipient
        FROM notification_recipients nrc
        INNER JOIN notifications nof
        ON nrc.notification_id = nof.notification_id
        WHERE (nrc.status = 'F' OR nrc.status = 'P') AND nrc.active = 'Y'
        AND nrc.notification_id = :notification_id AND nof.type = 'P'";

        $recipients = Yii::$app->db
            ->createCommand($sql)
            ->bindValues([':notification_id' => $notification->notification_id])
            ->queryAll();
        if (!$this->validateConfigurations()) {
            $returnValue = false;
            foreach (
                $notification->notificationRecipients
                as $notificationRecipent
            ) {
                $notificationRecipent->status = 'F';
                $notificationRecipent->status_information = $this->errorMessage;
                $notificationRecipent->save(false);
            }
        } else {
            if (!empty($recipients)) {
                foreach ($recipients as $recipient) {
                    $idRecord = $recipient['notification_recipient_id'];
                    $message = [
                        'en' => $notification->message,
                        'es' => $notification->message
                    ];
                    $options = [
                        'include_player_ids' => [$recipient['recipient']],
                        'data' => [
                            'type' => 'message'
                        ],
                        'android_group' => 'message',
                        'small_icon' => $notification->small_icon,
                        'large_icon' => $notification->big_icon,
                        'buttons' => !empty($notification->buttons)
                            ? (array) json_decode($notification->buttons)
                            : null
                    ];
                    $returnState = $this->onesignal
                        ->notifications()
                        ->create($message, $options);
                    if (!empty($returnState['errors'])) {
                        $statusRecord[] = [
                            'id' => $idRecord,
                            'info' => $returnState['errors'][0],
                            'status' => 'F'
                        ];
                    } else {
                        $statusRecord[] = [
                            'id' => $idRecord,
                            'info' => 'Sent successful',
                            'status' => 'S'
                        ];
                    }
                }
                if (!self::updateStateNotification($statusRecord)) {
                    $returnValue = false;
                }
            } else {
                $returnValue = true;
            }
        }
        return $returnValue;
    }

    /**
     * Metodo para actualizar el estado de los registros de notificación.
     *
     * @param $data array | datos de las notificaciones, estado y errores a registrar.
     * @return boolean
     */
    public function updateStateNotification($data = [])
    {
        $returnValue = true;
        if (!empty($data)) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($data as $statusRecord) {
                    if ($statusRecord['status'] == 'F') {
                        $returnValue = false;
                    }
                    $modelRecipient = NotificationRecipients::findOne([
                        'notification_recipient_id' => $statusRecord['id']
                    ]);
                    $modelRecipient->status = $statusRecord['status'];
                    $modelRecipient->status_information = $statusRecord['info'];
                    $modelRecipient->save(false);
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $returnValue = false;
                throw $e;
            }
        }
        return $returnValue;
    }

    /**
     * Valida las configuraciones para el canal.
     *
     * @param booelan $throwError Indica si se quiere lanzar el error como una excepción en la aplicación.
     * @return boolean
     */
    public function validateConfigurations($throwError = false)
    {
        $onesignalConfiguration = $this->module->onesignalConfig;
        $isValid = true;
        if (
            !(
                !is_null($onesignalConfiguration) &&
                isset($onesignalConfiguration['class'])
            )
        ) {
            $isValid = false;
            $this->errorMessage = Yii::t(
                $this->module->id,
                "It is necessary to define the configuration of the mail component and the instantiator class of this."
            );
        }
        if ($throwError && !$isValid) {
            if (!(Yii::$app instanceof \yii\console\Application)) {
                Yii::$app->session->setFlash(
                    $this->module->id . '-error',
                    $this->errorMessage
                );
            }
        }
        return $isValid;
    }
}
