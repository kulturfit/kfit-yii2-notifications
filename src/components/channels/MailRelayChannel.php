<?php

namespace kfit\notifications\components\channels;

use kfit\notifications\models\base\Notifications;
use Yii;

/**
 * Canal para manejar el envío de notificaciones por correo electrónico via el email
 * consumiento el servicio de MailRelay.
 *
 * @package kfit
 * @subpackage notifications\components\channels
 * @category components
 *
 * @property string $apiKey Token de acceso al servicio de mailrelay.
 * @property string $hostName Nombre de host suministrado por mailrelay.
 *
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  Daniel Julian Sanchez Alvarez <danieljulian9865@gmail.com>
 * @author  kevin Daniel Guzman Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2020 KulturFit S.A.S.

 */
class MailRelayChannel extends
    \kfit\notifications\components\channels\BaseEmailChannel
{
    public $apiKey;
    public $hostName;

    /**
     * Constructor de la clase
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        if (is_null($this->module->mailrelayConfig)) {
            throw new \yii\base\InvalidConfigException(
                Yii::t(
                    $this->module->id,
                    "It's necessary to define the configuration to the mailer component."
                )
            );
        }
        $this->mailer = Yii::createObject([
            'class' => '\kfit\notifications\components\mailrelay\Mailer',
            'apiKey' => $this->apiKey,
            'hostName' => $this->hostName
        ]);
    }

    /**
     * Método para el envío de los datos
     *
     * @param Notifications $notification
     * @return void
     */
    public function send(Notifications $notification)
    {
        $isOk = true;
        foreach (
            $notification->notificationRecipients
            as $notificationRecipent
        ) {
            if ($notificationRecipent->status != 'S') {
                if ($notificationRecipent->validate()) {
                    $mailInstance = $this->mailer->compose();
                    $this->mailer->htmlLayout = $this->htmlLayout;
                    $mailInstance->mailer->html = $this->mailer->render(
                        $this->template,
                        array_merge(
                            $this::getParamsEmail(
                                $notification,
                                $notificationRecipent
                            ),
                            [
                                'message' => $mailInstance
                            ]
                        )
                    );
                    $mailInstance
                        ->setTo($notificationRecipent->recipient)
                        ->setSubject($notification->title);
                    $mailInstance->mailer->hostName = $this->hostName;
                    $mailInstance->mailer->apiKey = $this->apiKey;
                    $notificationRecipent->status = $mailInstance->send()
                        ? 'S'
                        : 'F';
                    $notificationRecipent->status_information =
                        $mailInstance->errorMessage;
                    $notificationRecipent->save(false);
                    $isOk &= $notificationRecipent->status == 'S';
                } else {
                    $errors = $notificationRecipent->getErrors('recipient');
                    $notificationRecipent->load(
                        [
                            'status' => 'F',
                            'status_information' => $errors[0]
                        ],
                        ''
                    );
                    $notificationRecipent->save(false);
                }
            }
        }
        return $isOk;
    }
}
