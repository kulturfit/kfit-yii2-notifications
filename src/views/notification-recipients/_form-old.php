<?php

use kartik\select2\Select2;
use kfit\adm\models\base\Users;
use kfit\core\widgets\ActiveForm;
use kfit\notifications\helpers\Strings;
use kfit\notifications\models\app\Notifications;

/* @var $this yii\web\View */
/* @var $model kfit\notifications\models\base\NotificationRecipients */
/* @var $form kfit\core\widgets\ActiveForm */

?>

<div class="notification-recipients-form">
    <?php $form = ActiveForm::begin([
    'id' => 'notification-recipients-form',
]);?>

    <div class="row">
        <?=$form->field($model, 'notification_id')->hiddenInput()->label(false)?>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=$form->field($model, 'recipient', ['help' => '', 'popover' => $model->getHelp('recipient')])->textInput(['maxlength' => true, 'tabindex' => 1])?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=$form->field($model, 'status', ['help' => '', 'popover' => $model->getHelp('status')])->widget(
    Select2::classname(),
    [
        'data' => Notifications::getStatus(),
        'options' => ['placeholder' => !$model->isNewRecord ? Strings::getTextEmpty() : false, 'tabindex' => 2, 'disabled' => $model->isNewRecord],
    ]
)?>
        </div>
        <div class="col-12">
            <?=$form->field($model, 'status_information', ['help' => '', 'popover' => $model->getHelp('status_information')])->textarea(['rows' => 3, 'tabindex' => 3, 'disabled' => $model->isNewRecord])?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <?=$form->field($model, 'user_id', ['help' => '', 'popover' => $model->getHelp('user_id')])->widget(
    Select2::classname(),
    [
        'data' => Users::getData(),
        'options' => ['placeholder' => Strings::getTextEmpty(), 'tabindex' => 4],
    ]
)?>

        </div>


    </div>

    <?php ActiveForm::end();?>
</div>