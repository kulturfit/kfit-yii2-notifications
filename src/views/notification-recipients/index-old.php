<?php

use kartik\dynagrid\DynaGrid;
use kartik\grid\GridView;
use kfit\adm\models\app\Users;
use kfit\notifications\helpers\Strings;
use kfit\notifications\models\app\Notifications;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel kfit\notifications\models\searchs\NotificationRecipients */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="notification-recipients-index">
        <?php echo DynaGrid::widget([

    'columns' => [
        ['class' => 'kartik\grid\SerialColumn'],
        [
            'attribute' => 'notification_recipient_id',
            'visible' => false,
        ],
        [
            'attribute' => 'recipient',
            'visible' => true,
        ],
        [
            'attribute' => 'user_id',
            'visible' => false,
        ],
        [
            'attribute' => 'status',
            'visible' => true,
            'value' => function ($model) {
                switch ($model->status) {
                    case 'S':
                        return Yii::t($model->module->id, 'Sent');
                        break;
                    case 'F':
                        return Yii::t($model->module->id, 'Failed');
                        break;
                    case 'P':
                        return Yii::t($model->module->id, 'Pending');
                        break;
                }
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Notifications::getStatus(),
            'filterInputOptions' => [
                'id' => 'status_grid',
                'placeholder' => Strings::getTextAll(),
            ],
        ],
        [
            'attribute' => 'status_information',
            'format' => 'ntext',
            'visible' => false,
        ],
        [
            'attribute' => 'active',
            'visible' => false,
            'value' => function ($model) {
                return Strings::getCondition($model->active);
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Strings::getCondition(),
            'filterInputOptions' => ['id' => 'active_grid', 'placeholder' => Strings::getTextAll()],
            'hiddenFromExport' => true,
        ],
        [
            'attribute' => 'created_by',
            'visible' => false,
            'value' => function ($model) {
                return $model->creadoPor->username;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Users::getData(),
            'filterInputOptions' => ['id' => 'created_by_grid', 'placeholder' => Strings::getTextAll()],
            'hiddenFromExport' => true,
        ],
        [
            'attribute' => 'created_at',
            'visible' => false,
            'filterType' => GridView::FILTER_DATE,
            'filterInputOptions' => ['id' => 'created_at_grid'],
            'hiddenFromExport' => true,
        ],
        [
            'attribute' => 'updated_by',
            'visible' => false,
            'value' => function ($model) {
                return $model->modificadoPor->username;
            },
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => Users::getData(),
            'filterInputOptions' => ['id' => 'updated_by_grid', 'placeholder' => Strings::getTextAll()],
            'hiddenFromExport' => true,
        ],
        [
            'attribute' => 'updated_at',
            'visible' => true,
            'filterType' => GridView::FILTER_DATE,
            'filterInputOptions' => ['id' => 'updated_at_grid'],
            'hiddenFromExport' => true,
            'label' => Yii::t(Yii::$app->controller->module->id, 'Last attempt to send'),
        ],
        [
            'class' => 'kfit\core\base\ActionColumn',
            'isModal' => true,
            'dynaModalId' => 'notification-recipients',
            'urlCreator' => function ($action, $model, $key, $index) {
                return Url::to(["notification-recipients/{$action}", 'id' => $model->notification_recipient_id]);
            },
        ],
    ],
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'toolbar' => [
            '{toggleData}',
            [
                'content' =>
                Yii::$app->ui::btnNew('', ['notification-recipients/create', 'notification_id' => $model->notification_id], ['onClick' => "openModalGrid(this, 'notification-recipients', 'create'); return false;"]) . ' '
                . Yii::$app->ui::btnRefresh('', ['default/update', 'id' => $model->notification_id]),

            ],
            '{dynagrid}',
            '{export}',
        ],
        'pjax' => true,
        'options' => ['id' => 'gridview-notification-recipients'],
    ],
    'options' => ['id' => 'dynagrid-notification-recipients'],
]) ?>
        </div>
<?php Modal::begin([
    'id' => 'search-modal',
    'title' => Yii::t($searchModel->module->id, 'Búsqueda Avanzada'),
    'size' => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModal(Yii::t($searchModel->module->id, 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(Yii::t($searchModel->module->id, 'Buscar'), ['form' => 'notification-recipients-search-form']),
    'options' => ['style' => 'display: none;', 'tabindex' => false],
]);
echo $this->render('_search', ['model' => $searchModel]);
Modal::end();
?>
<?php Modal::begin([
    'id' => 'notification-recipients-modal',
    'title' => Yii::t($searchModel->module->id, 'Notification Recipients'),
    'size' => Modal::SIZE_LARGE,
    'footer' => Yii::$app->ui::btnCloseModalMessage(Yii::t($searchModel->module->id, 'Cancelar'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSend(null, ['form' => 'notification-recipients-form']),
    'closeButton' => ['class' => 'close confirm-close'],
    'options' => ['style' => 'display: none;', 'tabindex' => false],
]);

Modal::end();
?>
