<?php

use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use kfit\adm\models\app\Users;
use kfit\core\widgets\ActiveForm;
use kfit\notifications\helpers\Strings;
use kfit\notifications\models\app\Notifications;

/* @var $this yii\web\View */
/* @var $model kfit\notifications\models\searchs\NotificationRecipients */
/* @var $form kfit\core\widgets\ActiveForm */
?>

<div class="notification-recipients-search">
    <?php $form = ActiveForm::begin([
    'id' => 'notification-recipients-search-form',
    'action' => ['index'],
    'method' => 'get',
]);?>

    <div class="row">

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'notification_id', ['help' => '', 'popover' => $model->getHelp('notification_id')])
->widget(Select2::classname(),
    [
        'data' => Notifications::getData(),
        'options' => ['placeholder' => Strings::getTextAll(), 'tabindex' => 1, 'id' => 'notification_id_search'],
    ]
)?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'recipient', ['help' => '', 'popover' => $model->getHelp('recipient')])
->textInput(['tabindex' => 2, 'id' => 'recipient_search'])?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'user_id', ['help' => '', 'popover' => $model->getHelp('user_id')])->textInput(['tabindex' => 3, 'id' => 'user_id_search'])?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'status', ['help' => '', 'popover' => $model->getHelp('status')])
->textInput(['tabindex' => 4, 'id' => 'status_search'])?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'status_information', ['help' => '', 'popover' => $model->getHelp('status_information')])
->textInput(['tabindex' => 5, 'id' => 'status_information_search'])?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'active', ['help' => '', 'popover' => $model->getHelp('active')])
->widget(Select2::classname(),
    [
        'data' => Strings::getCondition(),
        'options' => ['placeholder' => Strings::getTextAll(), 'tabindex' => 6, 'id' => 'active_search'],
    ]
)?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'created_by', ['help' => '', 'popover' => $model->getHelp('created_by')])
->widget(Select2::classname(),
    ['data' => Users::getData(), 'options' => ['placeholder' => Strings::getTextEmpty(), 'tabindex' => 7, 'id' => 'created_by_search']]
)?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'created_at', ['help' => '', 'popover' => $model->getHelp('created_at')])
->widget(DatePicker::classname(), ['options' => ['tabindex' => 8, 'id' => 'created_at_search']])?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'updated_by', ['help' => '', 'popover' => $model->getHelp('updated_by')])
->widget(Select2::classname(),
    ['data' => Users::getData(), 'options' => ['placeholder' => Strings::getTextEmpty(), 'tabindex' => 9, 'id' => 'updated_by_search']]
)?>

			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<?=$form->field($model, 'updated_at', ['help' => '', 'popover' => $model->getHelp('updated_at')])
->widget(DatePicker::classname(), ['options' => ['tabindex' => 10, 'id' => 'updated_at_search']])?>

			</div>

    </div>


    <?php ActiveForm::end();?>
</div>
