<?php

use kfit\notifications\helpers\Strings;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model kfit\notifications\models\base\NotificationRecipients */

 ?>
<div class="notification-recipients-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'notification_recipient_id',
            [
                'attribute' => 'notification_id',
                'value'     => function ($model, $widget){
                    $name = $model->notification->getNameFromRelations();

                    return $model->notification->$name;
                },
            ],
            'recipient',
            'user_id',
            'status',
            'status_information:ntext',
            [
                'attribute' => 'active',
                'value'     => Strings::getCondition($model->active),
            ],
            [
                'attribute' => 'created_by',
                'value'     => $model->creadoPor->username,
            ],
            'created_at',
            [
                'attribute' => 'updated_by',
                'value'     => $model->modificadoPor->username,
            ],
            'updated_at',
        ],
    ]) ?>

</div>
